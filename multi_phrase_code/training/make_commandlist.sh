o="../../models/"
c1='config_one_linear_linearact';
c2='config_random_parse_tanh';
c3='config_none_linear_linearact';
c4='config_none_parse_linearact';
c5='config_one_parse_linearact';
c6='config_random_linear_tanh';


python make_tti_commands_single.py ../../core_data/phrase_training_data_60k.txt.multi.mat phrase_60k 0.01 $c2 $o > phrase_60k_test_c2.txt
python make_tti_commands_single.py ../../core_data/phrase_training_data_60k.txt.multi.mat phrase_60k 1.0 $c2 $o > phrase_60k_all_c2.txt

python make_tti_commands_single.py ../../core_data/phrase_training_data_60k.txt.multi.mat phrase_60k 0.01 $c4 $o > phrase_60k_test_c4.txt
python make_tti_commands_single.py ../../core_data/phrase_training_data_60k.txt.multi.mat phrase_60k 1.0 $c4 $o > phrase_60k_all_c4.txt

python make_tti_commands_single.py ../../core_data/phrase_training_data_60k.txt.multi.mat phrase_60k 0.01 $c5 $o > phrase_60k_test_c5.txt
python make_tti_commands_single.py ../../core_data/phrase_training_data_60k.txt.multi.mat phrase_60k 1.0 $c5 $o > phrase_60k_all_c5.txt
