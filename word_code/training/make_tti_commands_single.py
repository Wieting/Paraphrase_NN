import sys
import os

data = sys.argv[1]
fname = sys.argv[2]
frac = sys.argv[3]
config = sys.argv[4]
outfile = sys.argv[5]


lambda2=[0.01,0.001,0.0001,0.00001,0.000001,0.0000001,0]
bs=[100, 250, 500, 1000]

for j in range(len(bs)):
    for i in range(len(lambda2)):
        cmd = "0, "+str(lambda2[i])+", "+frac+", '"+fname+"', '"+data+"', "+str(bs[j])+", '"+config+"'"
        cmd = "/opt/matlab-r2013a/bin/matlab -nodisplay -nodesktop -nojvm -nosplash -r \"train_single("+cmd+");quit\""
        print cmd