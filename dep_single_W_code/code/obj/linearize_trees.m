function [ sample ] = linearize_trees( sample, theta, hiddenSize, We_orig )

for i=1:1:length(sample)
   t1 = sample{i}{1};
   t2 = sample{i}{2};
   
   t1 = fix_kids(t1);
   t2 = fix_kids(t2);
   
   sample{i}{1}=t1;
   sample{i}{2}=t2;
end

sample = feedForwardTrees(sample, theta, hiddenSize, We_orig);

end

function [t] = fix_kids(t)
    arr = t.kids;    
    flag = 1;
    kid = 1;
    
    for i=1:1:length(arr)
       if(arr(i,1)==0)
           continue;
       elseif(flag > 0)
           arr(i,:)=[1,2];
           flag = 0;
           kid = 3;
       else
           arr(i,:)= [i-1,kid];
           kid=kid+1;
       end
    end
    
    t.kids = arr;
end
