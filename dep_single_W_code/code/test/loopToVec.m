
hiddenSize = 3;
tensor = rand(hiddenSize,hiddenSize,hiddenSize);
vector = rand(hiddenSize,1);

mat = zeros(hiddenSize,hiddenSize);
%mat = repmat(vector,[1 hiddenSize]).*sum(tensor,3);
for i=1:1:hiddenSize
    for j=1:1:hiddenSize
        der = 0;
        for k=1:1:hiddenSize
            v = vector(k)*tensor(i,j,k);
            der = der + v;
        end
        mat(i,j) = der;
    end
end

mat2 = zeros(hiddenSize,hiddenSize);
v = reshape(vector,[1,1,hiddenSize]);
v = repmat(v,[hiddenSize hiddenSize 1]);

t = v.*tensor;
mat2 = sum(t,3);


matrix = rand(hiddenSize,hiddenSize);
mat = zeros(hiddenSize,1);
%mat = repmat(vector,[1 hiddenSize]).*sum(tensor,3);
for i=1:1:hiddenSize
    der = 0;
    for k=1:1:hiddenSize
        v = vector(k)*matrix(i,k);
        der = der + v;
    end
    mat(i) = der;
end

mat2 = zeros(hiddenSize,1);
v = reshape(vector,[1,hiddenSize]);
v = repmat(v,[hiddenSize 1]);

t = v.*matrix;
mat2 = sum(t,2);


x = rand(hiddenSize,1);
x2 = x;
m1 = rand(hiddenSize,hiddenSize);
vector = rand(hiddenSize,1);
for i=1:1:hiddenSize
    for j=1:1:hiddenSize
        x(i) = x(i) - m1(j,i)*vector(j);
    end
end

v = repmat(vector,[1 hiddenSize]);

t = v.*m1;
x2 = x2 - sum(t,1)';