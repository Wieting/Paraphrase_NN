function numgrad = computeNumericalGradientWords(J, theta, x,p)
% numgrad = computeNumericalGradient(J, theta)
% theta: a vector of parameters
% J: a function that outputs a real-number. Calling y = J(theta) will return the
% function value at theta. 
  
% Initialize numgrad with zeros

x
p

nums = [];
for i=1:1:length(x)
    t1 = x{i}{1};
    t2 = x{i}{2};
    nums = [nums t1.nums];
    nums = [nums t2.nums];
end

for i=1:1:length(p)
    t1 = p{i}{1};
    t2 = p{i}{2};
    nums = [nums t1.nums];
    nums = [nums t2.nums];
end

nums = unique(nums);
indices = [];
for i=1:1:length(nums)
   indices = [indices (nums(i)-1)*25+1:nums(i)*25];
end

numgrad = zeros(size(theta));

EPSILON = 1E-4;

for i=1:1:length(indices)
    idx = indices(i);
    z = zeros(length(numgrad),1);
    z(idx) = EPSILON;
    JplusEps = J(theta + z);
    JminusEps = J(theta - z);
    numgrad(idx) = (JplusEps - JminusEps)/(2*EPSILON);
end





%% ---------------------------------------------------------------
end
