function [grad] = computeGradNewObjWords(map, default, d, pairs, nodeparams, hiddenSize, words, We_old, We_orig, lambda, margin, params)

[a b] = size(nodeparams);
if (a ==1 || b == 1)
    n = numel(nodeparams);
    v = n/length(keys(map));
    nodeparams = reshape(nodeparams,v,length(keys(map)));
end

revmap = containers.Map('KeyType','double', 'ValueType','any');
k = keys(map);
for i=1:1:length(keys(map))
    ss= k(i);
    ss=ss{1};
    revmap(map(ss)) = ss;
end
%lambda = 0;

grad = zeros(size(We_orig));
for t=1:1:length(d)
    t1 = d{t}{1};
    t2 = d{t}{2};
    
    t1 = forwardpassWordDer(nodeparams, t1, map, default, hiddenSize,We_orig,params);
    t2 = forwardpassWordDer(nodeparams, t2, map, default, hiddenSize,We_orig,params);
    p1 = pairs{t}{1};
    p2 = pairs{t}{2};
    p1 = forwardpassWordDer(nodeparams, p1, map, default, hiddenSize,We_orig,params);
    p2 = forwardpassWordDer(nodeparams, p2, map, default, hiddenSize,We_orig,params);
    
    g1 = t1.nodeFeaturesforward(:,end);
    g2 = t2.nodeFeaturesforward(:,end);
    v1 = p1.nodeFeaturesforward(:,end);
    v2 = p2.nodeFeaturesforward(:,end);
    d1 = margin - sum(g1.*g2) + sum(v1.*g1);
    d2 = margin - sum(g1.*g2) + sum(v2.*g2);
    gradWe = zeros(size(We_orig));
    
    if(d1 > 0 || d2 > 0)
        allwords = unique([t1.nums t2.nums p1.nums p2.nums]);
        t1 = getVderW(nodeparams,t1,hiddenSize,revmap,map,params);
        t2 = getVderW(nodeparams,t2,hiddenSize,revmap,map,params);
        
        if(d1 > 0)
            p1 = getVderW(nodeparams,p1,hiddenSize,revmap,map,params);
            for w=1:1:length(allwords)
                ww=allwords(w);
                m1 = zeros(hiddenSize);
                m2 = zeros(hiddenSize);
                m3 = zeros(hiddenSize);
                if(~isempty(t1.dnodeWe))
                    m1 = matrixLookup(t1.dnodeWe{end},ww,hiddenSize);
                elseif(t1.nums(1)==ww)
                    m1 = eye(hiddenSize);
                end
                if(~isempty(t2.dnodeWe))
                    m2 = matrixLookup(t2.dnodeWe{end},ww,hiddenSize);
                elseif(t2.nums(1)==ww)
                    m2 = eye(hiddenSize);
                end
                if(~isempty(p1.dnodeWe))
                    m3 = matrixLookup(p1.dnodeWe{end},ww,hiddenSize);
                elseif(p1.nums(1)==ww)
                    m3 = eye(hiddenSize);
                end
                
                for i=1:1:hiddenSize
                    for j=1:1:hiddenSize
                        gradWe(i,ww) = gradWe(i,ww) - m1(j,i)*t2.nodeFeaturesforward(j,end);
                        gradWe(i,ww) = gradWe(i,ww) - m2(j,i)*t1.nodeFeaturesforward(j,end);
                        gradWe(i,ww) = gradWe(i,ww) + m3(j,i)*t1.nodeFeaturesforward(j,end);
                        gradWe(i,ww) = gradWe(i,ww) + m1(j,i)*p1.nodeFeaturesforward(j,end);
                    end
                end
            end
        end
        
        if(d2 > 0)
            p2 = getVderW(nodeparams,p2,hiddenSize,revmap,map,params);
            for w=1:1:length(allwords)
                ww=allwords(w);
                m1 = zeros(hiddenSize);
                m2 = zeros(hiddenSize);
                m3 = zeros(hiddenSize);
                if(~isempty(t1.dnodeWe))
                    m1 = matrixLookup(t1.dnodeWe{end},ww,hiddenSize);
                elseif(t1.nums(1)==ww)
                    m1 = eye(hiddenSize);
                end
                if(~isempty(t2.dnodeWe))
                    m2 = matrixLookup(t2.dnodeWe{end},ww,hiddenSize);
                elseif(t2.nums(1)==ww)
                    m2 = eye(hiddenSize);
                end
                if(~isempty(p2.dnodeWe))
                    m3 = matrixLookup(p2.dnodeWe{end},ww,hiddenSize);
                elseif(p1.nums(1)==ww)
                    m3 = eye(hiddenSize);
                end
                
                for i=1:1:hiddenSize
                    for j=1:1:hiddenSize
                        gradWe(i,ww) = gradWe(i,ww) - m1(j,i)*t2.nodeFeaturesforward(j,end);
                        gradWe(i,ww) = gradWe(i,ww) - m2(j,i)*t1.nodeFeaturesforward(j,end);
                        gradWe(i,ww) = gradWe(i,ww) + m3(j,i)*t2.nodeFeaturesforward(j,end);
                        gradWe(i,ww) = gradWe(i,ww) + m2(j,i)*p2.nodeFeaturesforward(j,end);
                    end
                end
            end
        end
    end
    
    
    grad = grad + gradWe;
end

grad = grad/length(d) - lambda.*(We_old-We_orig);

end

function [currTree] = getVderW(nodeparams,currTree,hiddenSize,revmap, map, params)

arr = unique(currTree.nums);

sl = size(currTree.nums,2);
%populate dsigmoids
for s=1:1:2*sl-1
    %take dsigmoid
    currTree.dsigmoid{end+1} = params.dsigmoid(currTree.nodeFeaturesforward(:,s));
end

for s=sl+1:1:2*sl-1
    m = containers.Map(arr(1),zeros(hiddenSize,hiddenSize));
    for i=2:1:length(arr)
        m(arr(i))=zeros(hiddenSize,hiddenSize);
    end
    currTree.dnodeWe{end+1}= m;
end

%populate node forward tensors
for s=sl+1:1:2*sl-1
    ss=s-sl;
    kids = currTree.kids(s,:);
    kidleft = kids(1);
    kidright = kids(2);
    
    theta_ll = nodeparams(:,map(currTree.POS{s}));
    W1 = reshape(theta_ll(1:hiddenSize*hiddenSize),hiddenSize,hiddenSize);
    W2 = reshape(theta_ll(hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize),hiddenSize,hiddenSize);
    bw1 = reshape(theta_ll(2*hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize+hiddenSize),hiddenSize,1);
    
    if(kidleft <= sl)
        w = currTree.nums(kidleft);
        currTree.dnodeWe{ss}(w)=W1 .* repmat(currTree.dsigmoid{s},1,hiddenSize);
    end
    
    if(kidright <= sl)
        w = currTree.nums(kidright);
        currTree.dnodeWe{ss}(w)=currTree.dnodeWe{ss}(w) + W2 .* repmat(currTree.dsigmoid{s},1,hiddenSize);
    end
    
    for k=1:1:length(arr)
        w=arr(k);
        %check what needs to be done.
        if(kidleft > sl && kidright <= sl)
            mleft = currTree.dnodeWe{kidleft-sl}(w);
            if(any(mleft(:)))
                T1=reshape(mleft',[1 hiddenSize hiddenSize]);
                T2=repmat(T1,[hiddenSize,1]);
                T3=reshape(W1,[hiddenSize 1 hiddenSize]);
                T4 = repmat(T3, [1 hiddenSize 1]);
                mat = sum(T2 .* T4,3);
                currTree.dnodeWe{ss}(w) = currTree.dnodeWe{ss}(w) + bsxfun(@times, mat, reshape(currTree.dsigmoid{s},1,hiddenSize)');
            end
            
        elseif(kidright > sl && kidleft <= sl)
            mleft = currTree.dnodeWe{kidright-sl}(w);
            if(any(mleft(:)))
                T1=reshape(mleft',[1 hiddenSize hiddenSize]);
                T2=repmat(T1,[hiddenSize,1]);
                T3=reshape(W2,[hiddenSize 1 hiddenSize]);
                T4 = repmat(T3, [1 hiddenSize 1]);
                mat = sum(T2 .* T4,3);
                currTree.dnodeWe{ss}(w) = currTree.dnodeWe{ss}(w) + bsxfun(@times, mat, reshape(currTree.dsigmoid{s},1,hiddenSize)');
            end
        elseif(kidright > sl && kidleft > sl)
            mat1=zeros(hiddenSize,hiddenSize);
            mat2=zeros(hiddenSize,hiddenSize);
            mleft = currTree.dnodeWe{kidleft-sl}(w);
            if(any(mleft(:)))
                T1=reshape(mleft',[1 hiddenSize hiddenSize]);
                T2=repmat(T1,[hiddenSize,1]);
                T3=reshape(W1,[hiddenSize 1 hiddenSize]);
                T4 = repmat(T3, [1 hiddenSize 1]);
                mat1 = sum(T2 .* T4,3);
            end
            mleft = currTree.dnodeWe{kidright-sl}(w);
            if(any(mleft(:)))
                T1=reshape(mleft',[1 hiddenSize hiddenSize]);
                T2=repmat(T1,[hiddenSize,1]);
                T3=reshape(W2,[hiddenSize 1 hiddenSize]);
                T4 = repmat(T3, [1 hiddenSize 1]);
                mat2 = sum(T2 .* T4,3);
            end
            currTree.dnodeWe{ss}(w) = currTree.dnodeWe{ss}(w) + bsxfun(@times, mat1+mat2, reshape(currTree.dsigmoid{s},1,hiddenSize)');
        end
    end
end
end