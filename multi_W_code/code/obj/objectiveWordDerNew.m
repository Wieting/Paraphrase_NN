function [cost] = objectiveWordDerNew(map, nodeparams, hiddenSize, d, pairs, words, We_old, We,lambda, lambdawords, margin, toprint, params)

%assume trees are propagated forward
% = reshape(We,25,27);
[a b] = size(We);
if (a ==1 || b == 1) 
   n = numel(We);
   v = n/hiddenSize;
   We = reshape(We,hiddenSize,v);
end

[a b] = size(nodeparams);
if (a ==1 || b == 1) 
   n = numel(nodeparams);
   v = n/length(keys(map));
   nodeparams = reshape(nodeparams,v,length(keys(map)));
end

total = 0;
for ii=1:1:length(d)
    %fprintf('Computing cost for tree %d\n',ii);
    t1 = d{ii}{1};
    t2 = d{ii}{2};
    p1 = pairs{ii}{1};
    p2 = pairs{ii}{2};
    
    [t1, nodeparams] = forwardpassWordDer(nodeparams, t1, map, [], hiddenSize, We, params);
    [t2, nodeparams] = forwardpassWordDer(nodeparams, t2,  map, [], hiddenSize, We, params);
    
    [p1, nodeparams] = forwardpassWordDer(nodeparams, p1,  map, [], hiddenSize, We, params);
    [p2, nodeparams] = forwardpassWordDer(nodeparams, p2,  map, [], hiddenSize, We, params);
    
    g1 = t1.nodeFeaturesforward(:,end);
    g2 = t2.nodeFeaturesforward(:,end);
    
    v1 = p1.nodeFeaturesforward(:,end);
    v2 = p2.nodeFeaturesforward(:,end);
    
    d1 = margin - sum(g1.*g2) + sum(v1.*g1);
    d2 = margin - sum(g1.*g2) + sum(v2.*g2);

    if(toprint)
       % [sum(g1.*g2) sum(v1.*g1) sum(v2.*g2) d1 d2 words(t1.nums) words(t2.nums) words(p1.nums) words(p2.nums)]
    end

    if(d1 < 0)
        d1 = 0;
    end
    
    if(d2 < 0)
        d2 = 0;
    end
    
    total = total + d1 + d2;
    %total = total - sum(g1.*g2);
end

total = total / length(d);
total2 = sum(sum(nodeparams .^ 2));
total2 = total2 * lambda / 2;
total3 = sum(sum((We_old-We).^2)) * lambdawords / 2;
cost = total+total2+total3;
end