
function [NodeParams, cost] = AGWords_test(Nodemap, default, NodeParams, params, hiddenSize, words, We_orig)

We_old = We_orig;

[params.data, NodeParams] = feedForwardTrees(params.data, Nodemap, NodeParams, default, hiddenSize, We_orig);
[pairs, params.data] = getPairsBatch(params.data, words, params.batchsize);

fprintf('Initial cost: %d\n',objectiveWordDerNew(Nodemap, NodeParams, hiddenSize, params.data, pairs, words, We_old, We_orig, params.lambda_t,params.lambda_w, params.margin,1));

Gt = zeros(size(NodeParams));
Gw = zeros(size(We_orig));
delta = 1E-4;


for(i=1:1:params.epochs)
    j=1;
    params.data = params.data(randperm(numel(params.data)));
    while(j <= length(params.data))
        batch= {};       
        e = params.batchsize+j-1;
        if(length(params.data) < e)
            e=length(params.data);
        end
        for l=j:1:e
            batch(end+1)=params.data(l);
        end
        [batch, ~] = feedForwardTrees(batch, Nodemap, NodeParams, default, hiddenSize, We_orig);
        pairs = getPairs(batch, words);
        %[pairs, params.data] = getPairsBatch(params.data, words, params.batchsize);
        grad=computeGradNewObj(Nodemap, default, batch, pairs, NodeParams, hiddenSize, words, We_orig, params.lambda_t,params.margin);
        numgrad = computeNumericalGradient( @(x) objectiveWordDerNew(Nodemap, x, hiddenSize, batch, pairs, words, We_old, We_orig, params.lambda_t,params.lambda_w, params.margin,0), NodeParams(:),x,p);
        numgrad = reshape(numgrad,size(NodeParams));
        diff = scoreGradient(numgrad, grad)
        Gt = Gt + grad.^2;
        NodeParams = NodeParams - params.etat*grad./(sqrt(Gt)+delta);
        
        gradWords = computeGradNewObjWords(Nodemap, default, batch, pairs, NodeParams, hiddenSize, words, We_old, We_orig,params.lambda_w,params.margin);
        numgrad = computeNumericalGradient( @(x) objectiveWordDerNew(Nodemap, NodeParams, hiddenSize, batch, pairs, words, We_old, x, params.lambda_t,params.lambda_w, params.margin,0), We_orig(:));
        numgrad = reshape(numgrad,size(We_orig));
        diff = scoreGradient(numgrad, gradWords)
        Gw = Gw + gradWords.^2;
        %numgrad = computeNumericalGradient( @(x) objectiveWordDer(x, hiddenSize, batch, words, We_orig), theta);
        %scoreGradient(numgrad,grad)
        %[numgrad(1:5) grad(1:5)];
        j = j + params.batchsize;
        We_orig = We_orig - params.etaw*gradWords./(sqrt(Gw)+delta);       
    end
    
    [params.data, ~] = feedForwardTrees(params.data, Nodemap, NodeParams, default, hiddenSize, We_orig);
    %pairs = getPairs(params.data, words);
    [pairs, params.data] = getPairsBatch(params.data, words, params.batchsize);
    cost = objectiveWordDerNew(Nodemap, NodeParams, hiddenSize, params.data, pairs, words, We_old, We_orig, params.lambda_t,params.lambda_w, params.margin,1);
    fprintf('cost at epoch %i : %d\n',i,cost);
end

%cost = costf(theta);

end
