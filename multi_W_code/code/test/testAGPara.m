clear;
addpath('../core');
addpath('../obj');
wef = 'data/skipwiki25.mat';
initv = 'data/theta_init_25.mat';
%dataf='../../../core_data/ppdb_test.txt.multi.mat';
dataf = '../../../core_data/phrase_training_data_60k.txt.multi.mat';
hiddenSize=25;
load(initv);
load(wef);
load(dataf);

play_data = [train_data test_data valid_data];
play_data = play_data(1:1000);
p = randperm(length(play_data));
play_data=play_data(p);

sample = play_data;
Nodemap = containers.Map();
NodeParams = [];
params.istest=1;
params.sigmoid=@sigmoid_linear;
params.dsigmoid=@dsigmoid_linear;
[sample, NodeParams] = feedForwardTrees(sample, Nodemap, NodeParams, theta, hiddenSize, We_orig, params);

params.lambda_t=0.01;
params.lambda_w=0.00001;
params.margin=1;
params.data = sample;
params.batchsize = 5;
params.epochs = 5;
params.etat=0.05;
params.etaw=0.5;
params.evaluate=1;
params.quiet=0;
params.save=1;
AGParaWords(Nodemap, theta, NodeParams, params, hiddenSize, words, We_orig,'test');