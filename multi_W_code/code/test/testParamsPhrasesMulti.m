function [NodeParams] = testParamsPhrasesMulti(fname,Nodemap, NodeParams, theta,We_orig,hiddenSize,words,params)
    [~,fname,~] = fileparts(fname);
    [acc, NodeParams] = phrasesMulti(Nodemap, NodeParams,theta,We_orig,hiddenSize,words,params);
    fprintf('phrases.......%s.......%d.......%d\n',fname,acc(1),acc(2));
end
