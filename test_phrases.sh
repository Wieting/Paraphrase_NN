#test word code
echo "Testing phrase code"
cd phrase_code/training/
alias matlab=/Applications/MATLAB_R2014a.app/bin/matlab
matlab -nodisplay -nodesktop -nojvm -nosplash -r "train_single(0, 0.01, 0.05, 100, '../../../Paraphrase_Project_data/core_data/phrase_training_data_60k.txt.mat', 'phrase_60k', 'config1.m');quit"

cd ../..
