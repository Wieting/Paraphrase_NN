load('../../../core_data/theta_init_25.mat');
theta_old = theta;

hiddenSize=25;
W1 = reshape(theta(1:hiddenSize*hiddenSize),hiddenSize,hiddenSize);
W2 = reshape(theta(hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize),hiddenSize,hiddenSize);
bw1 = reshape(theta(2*hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize+hiddenSize),hiddenSize,1);

W1 = eye(25);
W2 = eye(25);
bw1 = zeros(size(bw1));
theta = [W1(:); W2(:); bw1(:)];

save('../../../core_data/theta_init_ones_25.mat', 'theta');
theta = theta + theta_old;
save('../../../core_data/theta_init_ones_noise_25.mat', 'theta');
