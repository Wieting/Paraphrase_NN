from rungrid import *

file = open('phrase_60k_test_c2.txt','r')
lines = file.readlines()

lis1 = ['bronte.cs.illinois.edu']*12
lis2 = ['austen.cs.illinois.edu']*12
lis3 = ['shelley.cs.illinois.edu']*12
lis4 = ['smaug.cs.illinois.edu']*6

all_nodes = lis1 + lis2 + lis3 + lis4

jobs = []

for i in lines:
    #print i.strip()
    i=i.strip()
    arr = i.split()
    d1=arr[6].replace("\"train_single(","")
    d1=d1.replace(",","")
    d2 = arr[7].replace(",","")
    d3 = arr[9].replace(",","")
    c = arr[12].replace("');quit\"","").replace("'config_","")
    outfile = c+"_"+d1+"_"+d2+"_"+d3+".txt"
    cmd = i+" > "+outfile
    print cmd
    jobs.append(cmd)

print len(jobs)
grid = Grid(all_nodes,jobs)
grid.go()
#grid = Grid(['bronte.cs.illinois.edu', 'austen.cs.illinois.edu', 'shelley.cs.illinois.edu','morgoth.cs.illinois.edu','smaug.cs.illinois.edu'], lines)
#grid.go()
