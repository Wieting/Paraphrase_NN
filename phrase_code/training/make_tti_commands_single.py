import sys
import os

data = sys.argv[1]
fname = sys.argv[2]
frac = sys.argv[3]
config = sys.argv[4]
outfile = sys.argv[5]

lambda1 = [0, 0.1, 0.01, 0.001]
lambda2 = [10, 1, 0.1, 0.01, 0.001, 0.0001, 0.00001]
batchsize = [100,250,500,1000,2000]

for i in lambda1:
    for j in lambda2:
        for k in batchsize:
            cmd = str(i)+", "+str(j)+", "+frac+", "+str(k)+", '"+data+"', '"+fname+"', '"+config+"'"
            cmd = "matlab -nodisplay -nodesktop -nojvm -nosplash -r \"train_single("+cmd+");quit\""
            print cmd