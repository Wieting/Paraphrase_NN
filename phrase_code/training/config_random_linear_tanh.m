function [rnnoptions] = config_one_linear_linearact()
rnnoptions.hiddenSize=25;
rnnoptions.etat=.05;
rnnoptions.etaw=.5;
rnnoptions.margin=1;
rnnoptions.epochs=5;
rnnoptions.save=1;
rnnoptions.quiet=1;
rnnoptions.evaluate=1;
rnnoptions.sigmoid='sigmoid';
rnnoptions.dsigmoid='dsigmoid';
rnnoptions.linearize=1;
rnnoptions.wordfile='../../core_data/skipwiki25_1e-05_250_words-xl.params63.mat.ws353sl99.mat';
rnnoptions.init='../../core_data/theta_init_25.mat';
rnnoptions.output='../../models/SL999_random_linear_tanh';
end