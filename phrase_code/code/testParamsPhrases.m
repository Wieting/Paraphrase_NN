function [] = testParamsPhrases(fname,theta,We,hiddenSize,words,params)
    [~,fname,~] = fileparts(fname);
    acc = phrases(theta,We,hiddenSize,words,params);
    fprintf('phrases.......%s.......%d.......%d\n',fname,acc(1),acc(2));
end
