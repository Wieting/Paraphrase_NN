function [] = get_results(dfile)
%need to update core data and evaluation folders for each RNN directory (4
%of them. Evaluate All models for bigram, ppdb, and semeval on each.

addpath('../phrases/');
addpath('../bigrams/');
addpath('../../single_W_code/code/core/');
load('../../core_data/skipwiki25.mat');
load(dfile);
[hiddenSize,~]=size(We_orig);
[~,fname,~] = fileparts(dfile);
acc=phrases_nn(theta,We_orig,hiddenSize,words,'../../core_data/3000data.mat');
fprintf('ppdb_all\t%s\t%d\n',fname,acc);
acc=phrases_nn(theta,We_orig,hiddenSize,words,'../../core_data/ppdb_dev.txt.mat');
fprintf('ppdb_dev\t%s\t%d\n',fname,acc);
acc=phrases_nn(theta,We_orig,hiddenSize,words,'../../core_data/ppdb_test.txt.mat');
fprintf('ppdb_test\t%s\t%d\n',fname,acc);
acc=phrases_nn(theta,We_orig,hiddenSize,words,'../semeval/SICK_trial.txt.mat');
fprintf('sem_trial\t%s\t%d\n',fname,acc);
acc=phrases_nn(theta,We_orig,hiddenSize,words,'../semeval/SICK_test_annotated.txt.mat');
fprintf('sem_all\t%s\t%d\n',fname,acc);

%ML
acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/dev_verb-noun.txt');
fprintf('dev_verb-noun\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/dev_noun-noun.txt');
fprintf('dev_noun-noun\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/dev_adj-noun.txt');
fprintf('dev_adj-noun\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/test_verb-noun.txt');
fprintf('test_verb-noun\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/test_noun-noun.txt');
fprintf('test_noun-noun\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/test_adj-noun.txt');
fprintf('test_adj-noun\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/all_verb-noun.txt');
fprintf('all_verb-noun\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/all_noun-noun.txt');
fprintf('all_noun-noun\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/all_adj-noun.txt');
fprintf('all_adj-noun\t%s\t%d\n',fname,acc);

%ours all
acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/kj_vn.txt');
fprintf('all_kj_verb-noun\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/kj_nn.txt');
fprintf('all_kj_noun-noun\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/kj_an.txt');
fprintf('all_kj_adj-noun\t%s\t%d\n',fname,acc);



%%%%%%%%%%% dot

acc=phrases_nn(theta,We_orig,hiddenSize,words,'../../core_data/3000data.mat','dot');
fprintf('ppdb_all_dot\t%s\t%d\n',fname,acc);
acc=phrases_nn(theta,We_orig,hiddenSize,words,'../../core_data/ppdb_dev.txt.mat','dot');
fprintf('ppdb_dev_dot\t%s\t%d\n',fname,acc);
acc=phrases_nn(theta,We_orig,hiddenSize,words,'../../core_data/ppdb_test.txt.mat','dot');
fprintf('ppdb_test_dot\t%s\t%d\n',fname,acc);
acc=phrases_nn(theta,We_orig,hiddenSize,words,'../semeval/SICK_trial.txt.mat','dot');
fprintf('sem_trial_dot\t%s\t%d\n',fname,acc);
acc=phrases_nn(theta,We_orig,hiddenSize,words,'../semeval/SICK_test_annotated.txt.mat','dot');
fprintf('sem_all_dot\t%s\t%d\n',fname,acc);

%ML
acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/dev_verb-noun.txt','dot');
fprintf('dev_verb-noun_dot\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/dev_noun-noun.txt','dot');
fprintf('dev_noun-noun_dot\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/dev_adj-noun.txt','dot');
fprintf('dev_adj-noun_dot\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/test_verb-noun.txt','dot');
fprintf('test_verb-noun_dot\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/test_noun-noun.txt','dot');
fprintf('test_noun-noun_dot\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/test_adj-noun.txt','dot');
fprintf('test_adj-noun_dot\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/all_verb-noun.txt','dot');
fprintf('all_verb-noun_dot\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/all_noun-noun.txt','dot');
fprintf('all_noun-noun_dot\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/all_adj-noun.txt','dot');
fprintf('all_adj-noun_dot\t%s\t%d\n',fname,acc);

%ours all
acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/kj_vn.txt','dot');
fprintf('all_kj_verb-noun_dot\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/kj_nn.txt','dot');
fprintf('all_kj_noun-noun_dot\t%s\t%d\n',fname,acc);

acc = bigrams(theta,We_orig,hiddenSize,words,'../../evaluation/bigrams/data/kj_an.txt','dot');
fprintf('all_kj_adj-noun_dot\t%s\t%d\n',fname,acc);

end
