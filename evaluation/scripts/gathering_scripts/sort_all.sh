python gather_results.py > data.txt
sh sorter.sh ppdb_all
sh sorter.sh ppdb_test
sh sorter.sh ppdb_dev

sh sorter.sh sem_trial
sh sorter.sh sem_all

sh sorter.sh dev_verb-noun
sh sorter.sh dev_noun-noun
sh sorter.sh dev_adj-noun
sh sorter.sh test_verb-noun
sh sorter.sh test_noun-noun
sh sorter.sh test_adj-noun
sh sorter.sh all_verb-noun
sh sorter.sh all_noun-noun
sh sorter.sh all_adj-noun

sh sorter.sh all_kj_verb-noun
sh sorter.sh all_kj_noun-noun
sh sorter.sh all_kj_adj-noun

sh sorter.sh ppdb_all_dot
sh sorter.sh ppdb_test_dot
sh sorter.sh ppdb_dev_dot

sh sorter.sh sem_trial_dot
sh sorter.sh sem_all_dot

sh sorter.sh dev_verb-noun_dot
sh sorter.sh dev_noun-noun_dot
sh sorter.sh dev_adj-noun_dot
sh sorter.sh test_verb-noun_dot
sh sorter.sh test_noun-noun_dot
sh sorter.sh test_adj-noun_dot
sh sorter.sh all_verb-noun_dot
sh sorter.sh all_noun-noun_dot
sh sorter.sh all_adj-noun_dot

sh sorter.sh all_kj_verb-noun_dot
sh sorter.sh all_kj_noun-noun_dot
sh sorter.sh all_kj_adj-noun_dot