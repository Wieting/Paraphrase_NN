import sys

def getBestDevGivenTest(dev_str, test_str,lines):
    best_t=""
    best_d=""
    t=""
    max = -1
    for j in lines:
        i=j.split('\t')
        if(i[0]==dev_str and float(i[2])>max):
            best_d=i[1]
            max=float(i[2])
            t=j
    for i in lines:
        j=i.split('\t')
        if(j[0]==test_str and j[1]==best_d):
            best_t=i
    print t, best_t

f=open(sys.argv[1],'r')
lines=f.readlines()

getBestDevGivenTest('ppdb_dev','ppdb_test',lines)
getBestDevGivenTest('all_kj_verb-noun-36-dev','all_kj_verb-noun-36-test',lines)
getBestDevGivenTest('all_kj_adj-noun-36-dev','all_kj_adj-noun-36-test',lines)
getBestDevGivenTest('all_kj_noun-noun-36-dev','all_kj_noun-noun-36-test',lines)
getBestDevGivenTest('all_kj_verb-noun-54-dev','all_kj_verb-noun-54-test',lines)
getBestDevGivenTest('all_kj_adj-noun-54-dev','all_kj_adj-noun-54-test',lines)
getBestDevGivenTest('all_kj_noun-noun-54-dev','all_kj_noun-noun-54-test',lines)
getBestDevGivenTest('all_kj_verb-noun-72-dev','all_kj_verb-noun-72-test',lines)
getBestDevGivenTest('all_kj_adj-noun-72-dev','all_kj_adj-noun-72-test',lines)
getBestDevGivenTest('all_kj_noun-noun-72-dev','all_kj_noun-noun-72-test',lines)
getBestDevGivenTest('ppdb_all','all_kj_verb-noun',lines)
getBestDevGivenTest('ppdb_all','all_kj_noun-noun',lines)
getBestDevGivenTest('ppdb_all','all_kj_adj-noun',lines)
getBestDevGivenTest('ppdb_dev','all_kj_verb-noun',lines)
getBestDevGivenTest('ppdb_dev','all_kj_noun-noun',lines)
getBestDevGivenTest('ppdb_dev','all_kj_adj-noun',lines)
getBestDevGivenTest('sem_trial','sem_all',lines)
getBestDevGivenTest('dev_verb-noun','test_verb-noun',lines)
getBestDevGivenTest('dev_noun-noun','test_noun-noun',lines)
getBestDevGivenTest('dev_adj-noun','test_adj-noun',lines)
getBestDevGivenTest('ppdb_all','all_verb-noun',lines)
getBestDevGivenTest('ppdb_all','all_noun-noun',lines)
getBestDevGivenTest('ppdb_all','all_adj-noun',lines)
getBestDevGivenTest('ppdb_dev','all_verb-noun',lines)
getBestDevGivenTest('ppdb_dev','all_noun-noun',lines)
getBestDevGivenTest('ppdb_dev','all_adj-noun',lines)
getBestDevGivenTest('all_verb-noun','all_kj_verb-noun',lines)
getBestDevGivenTest('all_noun-noun','all_kj_noun-noun',lines)
getBestDevGivenTest('all_adj-noun','all_kj_adj-noun',lines)