function [accs] = phrases(theta,We_orig,hiddenSize,words,params)

c1=phrases_nn(theta,We_orig,hiddenSize,words,'../../core_data/ppdb_dev.txt.mat','cosine', params);
c2=phrases_nn(theta,We_orig,hiddenSize,words,'../../core_data/ppdb_test.txt.mat','cosine', params);
%c2=phrases_nn(theta,We_orig,hiddenSize,words,'../../evaluation/semeval/SICK_test_annotated.txt.mat');
%c2=0;

accs=[c1 c2];
%accs = c1;
end
