function [c, nodeparams]=phrases_nn_multi(Nodemap, nodeparams, theta,We,hiddenSize,words,dfile,params)

type = 'cosine';

load(dfile);
train_data = [train_data valid_data test_data];

x = [];
y = [];

for i=1:1:length(train_data)
    t1=train_data{i}{1};
    t2=train_data{i}{2};
    t1 = forwardpassWordDer(nodeparams, t1, Nodemap, theta, hiddenSize, We, params, 0);
    t2 = forwardpassWordDer(nodeparams, t2, Nodemap, theta, hiddenSize, We, params, 0);
    g1 = t1.nodeFeaturesforward(:,end);
    g2 = t2.nodeFeaturesforward(:,end);
    d=dot(g1,g2);
    norm(g1);
    norm(g2);
    [g1 g2];
    if(strcmp(type,'cosine')==1)
        d = dot(g1,g2)/(norm(g1)*norm(g2));
    else
        d = dot(g1,g2);
    end
    x = [x d];
    y = [y train_data{i}{3}];
end

[x; y];

[c,~] = corr(x',y','Type','Spearman');

end

