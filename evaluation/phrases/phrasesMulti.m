function [accs, NodeParams] = phrasesMulti(Nodemap, NodeParams, theta,We,hiddenSize,words,params)

W1 = eye(25);
W2 = eye(25);
bw1 = zeros(25,1);
iden = [W1(:); W2(:); bw1(:)];

c1=0;
c2=0;
if(params.istest)
    [c1, NodeParams]=phrases_nn_multi(Nodemap, NodeParams, iden,We,hiddenSize,words,'../../../core_data/ppdb_dev.txt.multi.mat',params);
    [c2, NodeParams]=phrases_nn_multi(Nodemap, NodeParams, iden,We,hiddenSize,words,'../../../core_data/ppdb_test.txt.multi.mat',params);
else
    [c1, NodeParams]=phrases_nn_multi(Nodemap, NodeParams, iden,We,hiddenSize,words,'../../core_data/ppdb_dev.txt.multi.mat',params);
    [c2, NodeParams]=phrases_nn_multi(Nodemap, NodeParams, iden,We,hiddenSize,words,'../../core_data/ppdb_test.txt.multi.mat',params);
end

%c2=phrases_nn(theta,We_orig,hiddenSize,words,'../../evaluation/semeval/SICK_test_annotated.txt.mat');
%c2=0;

accs=[c1 c2];
%accs = c1;
end
