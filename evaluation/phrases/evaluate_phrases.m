addpath('../../single_W_code/code/core/')
dfile = '../../../Paraphrase_Project_data/core_data/d200_scored.txt.mat';
sfile= '../../evaluation/semeval/SICK_test_annotated.txt.mat';
load('../../core_data/skipwiki25.mat');
%load('../../../Paraphrase_Project_data/core_data/skipwiki25_0.0001_100_words-xl.params70.mat');
%load('../../../Paraphrase_Project_data/core_data/skipwiki25_1e-05_100_words-xl.params63.mat');
%load('../../../Paraphrase_Project_data/core_data/skipwiki25_1e-05_250_words-xl.params63.mat');
%phrases_additive(We_orig,dfile)
%phrases_additive(We_orig,sfile)


load('../../models/skipwiki25_0_1e-06_2000_phrase_60k.params16.mat');
load('../../core_data/skipwiki25.mat');
phrases_additive(We_orig,dfile)
phrases_additive(We_orig,sfile)
accs = phrases_nn_ra(theta,We_orig,25,words,dfile)
accs = phrases_nn_ra(theta,We_orig,25,words,sfile)