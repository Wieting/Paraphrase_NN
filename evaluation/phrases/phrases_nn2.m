function [predicted, gold]=phrases_nn2(theta,We_orig,hiddenSize,words,dfile)

load(dfile);
train_data = [train_data valid_data test_data];

W1 = reshape(theta(1:hiddenSize*hiddenSize),hiddenSize,hiddenSize);
W2 = reshape(theta(hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize),hiddenSize,hiddenSize);
bw1 = reshape(theta(2*hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize+hiddenSize),hiddenSize,1);

x = [];
y = [];

for i=1:1:length(train_data)
    t1=train_data{i}{1};
    t2=train_data{i}{2};
    t1 = forwardpassWordDer(t1, W1, W2, bw1, hiddenSize, We_orig);
    t2 = forwardpassWordDer(t2, W1, W2, bw1, hiddenSize, We_orig);
    g1 = t1.nodeFeaturesforward(:,end);
    g2 = t2.nodeFeaturesforward(:,end);
    d=dot(g1,g2);
    norm(g1);
    norm(g2);
    [g1 g2];
    d = dot(g1,g2)/(norm(g1)*norm(g2));
    x = [x d];
    y = [y train_data{i}{3}];
end

predicted = x;
gold = y;

end