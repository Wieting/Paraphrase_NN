import ntpath
from glob import glob

files = '../../models/*.mat'

lines=[]
for file in glob(files):
    lines.append(file)

lambda1 = [0, 0.1, 0.01, 0.001]
lambda2 = [10, 1, 0.1, 0.01, 0.001, 0.0001, 0.00001]
batchsize = [100,250,500,1000,2000]

lis=[]
for i in lambda1:
    for j in lambda2:
        for k in batchsize:
            newl=str(i)+'_'+str(j)+'_'+str(k)
            lis.append(newl)


for i in lines:
    ff=ntpath.basename(i.strip())
    f=ff.split('_')
    param1=f[1]
    param2=f[2]
    param3=f[3]
    if('params20' in ff):
        newl=param1+'_'+param2+'_'+param3
        newl = newl.replace('-xl','')
        #print newl, newl in lis
        lis.remove(newl)

#for i in lis:
#    print i

for i in lis:
    i=i.split('_')
    p1=float(i[0])
    p2=float(i[1])
    p3=float(i[2])
    df='../../../Paraphrase_Project_data/core_data/phrase_training_data_60k.txt.mat'
    f='phrase_60k'
    cmd = str(p1)+", "+str(p2)+", 1.0, "+str(p3)+", '"+df+"', '"+f+"', 'config1.m'"
    cmd = "/opt/matlab-r2013a/bin/matlab -nodisplay -nodesktop -nojvm -nosplash -r \"train_single("+cmd+");quit\""
    print cmd

#/opt/matlab-r2013a/bin/matlab -nodisplay -nodesktop -nojvm -nosplash -r "train_single(0, 0.0001, 1.0, 1000, '../../../Paraphrase_Project_data/bigram_data/adj-noun-base-xl.txt.mat', 'adj-noun-xl',