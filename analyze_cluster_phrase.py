import sys
import operator

f = sys.argv[1]
best = 0
total = 0

d={}
missing=[]
for i in range(100):
    j = i + 1
    ff = f + "." + str(j)
    ff = open(ff,'r')
    lines = ff.readlines()
    ct =0
    for ll in lines:
        if('.......' in ll):
            arr = ll.split('.......')
            name = arr[1]+" "+arr[2]+" "+arr[3].strip()
            score = float(arr[2])
            d[name]=score
            test_score = float(arr[3])
            if(test_score > best):
                best = test_score
            total += 1
            ct += 1
    if(ct < 20):
        missing.append(f + "." + str(j)+" "+str(ct))
sorted_d = sorted(d.items(), key=operator.itemgetter(1))
for i in sorted_d:
    print i[0], i[1]
print best
print total
print missing
